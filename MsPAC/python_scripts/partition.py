#!/bin/env python
import os 
import sys
import vcf
import pysam
import numpy as np
from collections import Counter

def read_vcf_hap_dict (vcf_fn, sample, chrom):
    '''
    Read in the vcf to a dictionary keeping track of phased bases
    The key of the dictionary is a chrom and the value is 
    another dictionary of the form:
    pos -> [mat_base, pat_base]
    Only het phased bases are examined.
    '''
    hap_chrom_dict = {} # key = chrom, value = pos -> [mat_base, pat_base] dict
    vcf_reader = vcf.Reader(open(vcf_fn, 'r'))
    #records = vcf_reader.fetch(chrom)
    het_calls = 0
    tot_calls = 0
    try:
        records = vcf_reader.fetch(str(chrom))
    except:
        print "%s has no SNPs in %s" % (vcf_fn,chrom)
        exit(0)
    for record in records:
        hap_chrom_dict.setdefault(record.CHROM, {})
        alleles  = record.genotype(sample)['GT'].split("|")
        if len(alleles) == 2 and alleles[0] != alleles[1]:
            alleles = map(int, alleles)
            allele_bases = [record.REF] # create an indexed set based on the genotype
            allele_bases.extend(record.ALT)
            # the order of these bases is the order of the alleles which should be P|M
            sample_bases = map(lambda x: allele_bases[x], alleles)
            hap_chrom_dict[record.CHROM][record.POS-1] = sample_bases
            het_calls += 1
        tot_calls += 1
        if tot_calls % 100000 == 0:
            print >>sys.stderr, het_calls, tot_calls, record.CHROM
    return hap_chrom_dict
            
def calculate_prob (basetups, hap_dict, threshold=0.9, lr_threshold=10):
    '''
    Returns a 0, 1 or -1 as the value of a read
    '''
    log_lr_threshold = np.log(lr_threshold)
    prob0  = 1.
    prob1  = 1.
    lprob0 = 0.
    lprob1 = 0.

    for basetup in basetups:
        rpos, b, qual = basetup
        b0, b1 = hap_dict[rpos]
        #print "--", qual, b, b0, b1              
        if b == b1 or b == b0: # check to make sure the base is called
            if b == b0:
                prob0 *= (1.-10.**(-qual/10.))
                prob1 *= (10.**(-qual/10.))
                lprob0 += np.log(1.-10.**(-qual/10.))
                lprob1 += np.log(10.**(-qual/10.))
                                
            else:
                prob1 *= (1.-10.**(-qual/10.))
                prob0 *= (10.**(-qual/10.))
                lprob1 += np.log(1.-10.**(-qual/10.))
                lprob0 += np.log(10.**(-qual/10.))
        else:
            # for now we're computing a ratio, so we can just ignore these guys
            pass 
    prob01 = prob0 + prob1
    if (prob0 > 0 and prob1 > 0) and prob0 / (prob01) >= threshold:
        if lprob0 - lprob1 >= log_lr_threshold:
            #print>>sys.stderr, "match1"
            return create_tag(1)
        else:
            #print>>sys.stderr, "ratio differs from prob"
            pass
    if (prob0 > 0 and prob1 > 0) and prob1 / (prob01) >= threshold:
        if lprob1 - lprob0 >= log_lr_threshold:
            #print>>sys.stderr, "match2"
            return create_tag(2)
        else:
            #print>>sys.stderr, "ratio differs from prob"
            pass
    
    if (prob0 == 0 or prob1 == 0) and lprob0 - lprob1 >= log_lr_threshold:
        #print>>sys.stderr, "match1b"
        return create_tag(1)
        #print >>sys.stderr, "ratio differs from prob"
    if (prob0 == 0 or prob1 == 0) and lprob1 - lprob0 >= log_lr_threshold:
        print >>sys.stderr, "match2b"
        return create_tag(2)
        #print>>sys.stderr, "ratio differs from prob"
    #print>>sys.stderr, "epic fails"
    return create_tag(0)

def create_tag (hap):
    haptag = ("ZHT", hap, "i")
    return haptag
    
def bayesian_partitioner (outdir, b_fn, chrom, hap_dict):
    '''
    outdir = output tag for new bam_file
    b_fn = bam file or fofn
    chrom = chrom to look at in bam(s)
    hap_dict = haplotype calls for positions in chrom

    let, hap_bases be our set of bases we're interrogating

    P (r | H_1 ) = \pi_{i \in |hap_bases|} Pr(r_i | hap_bases[i])

    Where Pr(r_i | hap_bases[i]) == 1 - QV_i 

    Note, when there is not quality, just a phred score of 8
    '''
    import pysam
    bam_fns = []
    if b_fn[-3:] == "bam":
        bamfile = pysam.AlignmentFile(b_fn)
        bam_fns.append(b_fn)
    else:
        with open (b_fn) as f:
            for l in f:
                fn = l.strip()
                bam_fns.append(fn)
        bamfile = pysam.AlignmentFile(bam_fns[0])
    bamoutfile = pysam.AlignmentFile(outdir+"/out.bam", "wb", template=bamfile)
    bamoutfile_desert = pysam.AlignmentFile(outdir+"/snpdesert.bam", "wb", template=bamfile)
    bamoutfile1 = pysam.AlignmentFile(outdir+"/hap1.bam", "wb", template=bamfile)
    bamoutfile2 = pysam.AlignmentFile(outdir+"/hap2.bam", "wb", template=bamfile)
    min_var = min(hap_dict.keys())
    max_var = max(hap_dict.keys())
    hapCounts = Counter()
    read_counter = 0
    span_event = False    
    for bam_fn in bam_fns:
        bamfile = pysam.AlignmentFile(bam_fn)
        for read in bamfile.fetch(chrom, min_var, max_var):
            if read.is_secondary:
                continue
            basetups = []
            for ap in read.get_aligned_pairs():
                qpos, rpos = ap
                if rpos in hap_dict:
                    span_event = True                    
                    if qpos is not None:
                        if read.query_qualities == None:
                            qual = 8
                        else:
                            qual = read.query_qualities[qpos]
                        b = read.query_sequence[qpos]
                        basetups.append((rpos, b, qual))
            read_hap_tag = create_tag(0)
            if len(basetups) > 0:
                read_hap_tag = calculate_prob(basetups, hap_dict)
                #print len(basetups), read_hap_tag
                #read.set_tag(read_hap_tag[0], read_hap_tag[1], read_hap_tag[2])
            rtags = read.get_tags()
            rtags.append(read_hap_tag)
            read.set_tags(rtags)
            hapCounts[read_hap_tag[1]] += 1
            read_counter += 1
            # if read_counter % 1000 == 0:
            #     print read_counter, hapCounts
            #     print read.reference_start
            if read_hap_tag[1] == 1:
                bamoutfile1.write(read)
            elif read_hap_tag[1] == 2:
                bamoutfile2.write(read)
            elif read_hap_tag[1] == 0: # note this includes ALL reads that did not have a haplotype label
                bamoutfile_desert.write(read)
                
            if span_event:
                bamoutfile.write(read)
            #else:
            #    bamoutfile_desert.write(read)
            span_event = False

#split_bam_by_phased_snps(input_data)
def split_bam_by_phased_snps(data):
    bam_fn = data["aligned_reads_bam"]
    vcf_fn = data["phased_snps_vcf"]
    sample = data["vcf_sample_name"]
    input_chrom = data["chromosome"]
    outdir = "%s/partition/%s" % (data["output_directory"],input_chrom)
    hap_chrom_dict =  read_vcf_hap_dict(vcf_fn, sample, input_chrom)
    bayesian_partitioner(outdir, bam_fn, input_chrom, hap_chrom_dict[input_chrom])
