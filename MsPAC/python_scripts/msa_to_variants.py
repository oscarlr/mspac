#!/bin/env python
from MsPAC.python_scripts.hmm import *
from pomegranate import *
from Bio import AlignIO
import numpy as np

def get_msa_sequence(clufn):
    alignment = AlignIO.read(clufn,"clustal")
    sequences = {}
    for sequence in alignment:
        sequences[sequence.id] = str(sequence.seq).upper()
    return sequences

def get_observations(sequence,coord):
    obs = []
    if len(sequence) == 3:
        h1_seq = sequence["H1"]
        h2_seq = sequence["H2"]
        chrom_seq = sequence[coord]
        for h1,h2,chrom in zip(h1_seq,h2_seq,chrom_seq):
            obs.append(observations["3"][h1][h2][chrom])
        return obs
    if len(sequence) == 2:
        keys = sequence.keys()
        keys.remove(coord)
        hap = keys[0]
        hap_seq = sequence[hap]
        chrom_seq = sequence[coord]
        for h, chrom in zip(hap_seq,chrom_seq):
            obs.append(observations["2"][h][chrom])
        return obs

def three_sequence_msa_variants(sequence,path,clufn,coord,start):
    variant_type = None
    output_variants = []
    ref_pos = int(start)
    ref_index = 0
    start_index = 0
    flank = 1000
    for i,(h1,h2,ref,p) in enumerate(zip(sequence["H1"],sequence["H2"],sequence[coord],path[1:-1])):
        state_index, state = p
        #print i,state_index,state,h1,h2,ref
        if state.name != "NORMAL" and variant_type == None:
            variant_type = state.name
            variant_chrom = coord.split("_")[0]
            variant_ref_start = ref_pos
            variant_size = 1
            if ref != "-":
                variant_ref_seq = [ref]
            else:
                variant_ref_seq = []
            if h1 !="-":
                variant_h1_seq = [h1]
            else:
                variant_h1_seq = []
            if h2 !="-":
                variant_h2_seq = [h2]
            else:
                variant_h2_seq = []
            start_index = i
            extract_ref_index = ref_pos
        elif state.name == variant_type:
            variant_size += 1
            if ref != "-":
                variant_ref_seq.append(ref)
            if h1 !="-":
                variant_h1_seq.append(h1)
            if h2 !="-":
                variant_h2_seq.append(h2)
        elif state.name != variant_type and variant_type != None:
            variant_ref_end = ref_pos
            if len(variant_ref_seq) == 0:
                variant_ref_seq = "."
            if len(variant_h1_seq) == 0:
                variant_h1_seq = "."
            if len(variant_h2_seq) == 0:
                variant_h2_seq = "."
            variant_type,genotype = variant_type.split("_")
            extract_start = None
            if (start_index - flank - sequence[coord][start_index-flank:start_index].count("-")) >= 0:
                extract_start = start_index - flank - sequence[coord][start_index-flank:start_index].count("-")
                extract_ref_start = int(start) + (len(sequence[coord][0:extract_start]) - sequence[coord][0:extract_start].count("-"))
            else:
                extract_start = 0
                extract_ref_start = start
            extract_end = None
            if len(sequence["H1"]) > i + flank:
                extract_end = i + flank
            else:
                extract_end = len(sequence["H1"]) - 1
            matt_ref_seq = sequence[coord][extract_start:extract_end]
            matt_h1_seq = sequence["H1"][extract_start:extract_end]
            matt_h2_seq = sequence["H2"][extract_start:extract_end]
            if matt_ref_seq == "":
                print extract_start,extract_end
            assert matt_ref_seq != ""
            assert matt_h1_seq != ""
            assert matt_h2_seq != ""
            variant_ref_seq = "".join(variant_ref_seq)
            variant_h1_seq = "".join(variant_h1_seq)
            variant_h2_seq = "".join(variant_h2_seq)
            output_variants.append([variant_chrom,variant_ref_start,variant_ref_end,variant_type,genotype,variant_size,variant_ref_seq,variant_h1_seq,variant_h2_seq,clufn,extract_ref_start,matt_ref_seq,matt_h1_seq,matt_h2_seq,start_index])
            variant_type = None
        if ref != "-":
            ref_pos += 1
            ref_index += 1
    return output_variants

def two_sequence_msa_variants(sequence,path,clufn,chrom,start):
    variant_type = None
    output_variants = []
    ref_pos = int(start)
    ref_index = 0
    start_index = 0
    flank = 1000
    keys = sequence.keys()
    keys.remove(chrom)
    assert len(keys) == 1
    for i,(h,ref,p) in enumerate(zip(sequence[keys[0]],sequence[chrom],path[1:-1])):
        state_index, state = p
        if state.name != "NORMAL" and variant_type == None:
            variant_type = state.name
            variant_chrom = chrom
            variant_ref_start = ref_pos
            variant_size = 1
            if ref != "-":
                variant_ref_seq = [ref]
            else:
                variant_ref_seq = []
            if h !="-":
                variant_h_seq = [h]
            else:
                variant_h_seq = []
            start_index = i
            extract_ref_index = ref_pos
        elif state.name == variant_type:
            variant_size += 1
            if ref != "-":
                variant_ref_seq.append(ref)
            if h !="-":
                variant_h_seq.append(h)
        elif state.name != variant_type and variant_type != None:
            variant_ref_end = ref_pos
            if len(variant_ref_seq) == 0:
                variant_ref_seq = "."
            if len(variant_h_seq) == 0:
                variant_h_seq = "."
            extract_start = None
            if (start_index - flank - sequence[chrom][start_index-flank:start_index].count("-")) >= 0:
                extract_start = start_index - flank - sequence[chrom][start_index-flank:start_index].count("-")
                extract_ref_start = int(start) + (len(sequence[chrom][0:extract_start]) - sequence[chrom][0:extract_start].count("-"))
            else:
                extract_start = 0
                extract_ref_start = start
            if keys[0] == "H1":
                genotype = "1|."
                variant_h1_seq = "".join(variant_h_seq)
                variant_h2_seq = "."
                extract_end = None
                if len(sequence["H1"]) > i + flank:
                    extract_end = i + flank
                else:
                    extract_end = len(sequence["H1"]) - 1
                matt_h1_seq = sequence["H1"][extract_start:extract_end]
                matt_h2_seq = "."
            else:
                genotype = ".|1"
                variant_h2_seq = "".join(variant_h_seq)
                variant_h1_seq = "."
                extract_end = None
                if len(sequence["H2"]) > i + flank:
                    extract_end = i + flank
                else:
                    extract_end = len(sequence["H2"]) - 1
                matt_h1_seq = "."
                matt_h2_seq = sequence["H2"][extract_start:extract_end]
            matt_ref_seq = sequence[chrom][extract_start:extract_end]
            assert matt_ref_seq != ""
            assert matt_h1_seq != ""
            assert matt_h2_seq != ""
            output_variants.append([variant_chrom,variant_ref_start,variant_ref_end,variant_type,genotype,variant_size,variant_ref_seq,variant_h1_seq,variant_h2_seq,clufn,extract_ref_start,matt_ref_seq,matt_h1_seq,matt_h2_seq,start_index])
            variant_type = None
        if ref != "-":
            ref_pos += 1
            ref_index += 1
    return output_variants

def path_to_variants(path,sequence,coord,start,end,clufn):
    if len(sequence) == 3:
        variants = three_sequence_msa_variants(sequence,path,clufn,coord,start)
    elif len(sequence) == 2:
        variants = two_sequence_msa_variants(sequence,path,clufn,coord,start)
    for output_variant in variants:
        print "\t".join(map(str,output_variant))

def main():
    clufn = sys.argv[1]
    chrom = sys.argv[2]
    start = sys.argv[3]
    end = sys.argv[4]
    sequence = get_msa_sequence(clufn)
    coords = sequence.keys()
    if "H1" in coords:
        coords.remove("H1")
    if "H2" in coords:
        coords.remove("H2")
    coord = coords[0]
    obs = get_observations(sequence,coord)
    log, path = model[str(len(sequence))].viterbi(obs)
    path_to_variants(path,sequence,coord,start,end,clufn)   

if __name__ == "__main__":
    main()
