#!/bin/env python
import pysam
from Bio import SeqIO
import os
from MsPAC.python_scripts._util import save_intervals,create_dir,tmpl_bash,submit_bashfiles

def coverage_regions(output,bamfn,data):
    cov_min = data["min_reads"]
    intervals = []
    bamfile = pysam.AlignmentFile(bamfn.rstrip(),'rb')
    start_interval = -1
    end_interval = -1
    prev_end = 0
    for pileupcolumn in bamfile.pileup():
        tid = pileupcolumn.reference_id
        ref_name = bamfile.getrname(tid)
        if pileupcolumn.nsegments >= int(cov_min):
            p = pileupcolumn.pos
            if start_interval == -1:
                start_interval = p
            end_interval = p+1
        else:
            if start_interval != -1:
                interval = (ref_name, start_interval, end_interval)
                intervals.append(interval)
                start_interval, end_interval, prev_end = -1, -1, start_interval
    if start_interval != -1:
        interval = (ref_name, start_interval, end_interval)
        intervals.append(interval)
    save_intervals(intervals,"%s_coverage.bed" % output)
    return intervals

def split_window(start,end,window,padding):
    intervals = []
    # List of list of regions with original region split into ideal region sizes
    intervals_chunks_ = range(start,end,window)
    intervals_chunks = []
    for x,y in zip(intervals_chunks_[:-1],intervals_chunks_[1:]):
        intervals_chunks.append([x,y])
    if intervals_chunks[-1][-1] != end:
        intervals_chunks.append([intervals_chunks[-1][-1],end])
    # If the last intervals is small added it to the second to last interval
    # and remove the last interval
    if (intervals_chunks[-1][-1] - intervals_chunks[-1][0]) < (window/5):
        intervals_chunks[-2][-1] = intervals_chunks[-1][-1]
        intervals_chunks = intervals_chunks[:-1]
    # Add padding to the end of the first interval
    intervals.append((intervals_chunks[0][0],intervals_chunks[0][-1] + padding,intervals_chunks[0][0],intervals_chunks[0][-1]))
    # Loop through the intervals except for the last and first
    for interval_chunk in intervals_chunks[1:-1]:
        intervals.append((interval_chunk[0] - padding,interval_chunk[-1] + padding,interval_chunk[0],interval_chunk[-1]))
    # Add padding to the start of the last interval
    intervals.append((intervals_chunks[-1][0] - padding,intervals_chunks[-1][-1],intervals_chunks[-1][0],intervals_chunks[-1][-1]))
    return intervals

def split_regions_into_windows(output,coverage_regions,data):
    window_size = int(data["window_size"])
    flanking = int(data["flanking"])
    min_window_size = int(data["min_window_size"])
    intervals = []
    for chrom,start,end in coverage_regions:
        start = int(start)
        end = int(end)
        if (end - start) > int(window_size):
            split_intervals = split_window(start,end,window_size,flanking)
            for start,end,assembly_start,assembly_end in split_intervals:
                intervals.append((chrom,start,end))
        elif (end - start) > int(min_window_size):
            intervals.append((chrom,start,end))
    save_intervals(intervals,"%s_windows.bed" % output)
    return intervals

def assemble_intervals(interval_output,hap_windows,bamfn,data):
    bashfns = []
    for chrom,start,end in hap_windows:
        if chrom != data["chromosome"]:
            continue
        outdir = "%s/%s/%s_%s" % (interval_output,chrom,start,end)
        if data["assembly_type"] == "conservative":         
            if os.path.isfile("%s/no.raw.contigs" % outdir):
                continue
        if data["clean_contigs"] == "True":
            if os.path.isfile("%s/quiver.contigs.fastq" % outdir):
                continue
        else:
            if os.path.isfile("%s/raw.contigs.fasta" % outdir):
                continue
        create_dir(outdir)
        bashfn = "%s/assembly.sh" % outdir
        swap = { "bam_fn": bamfn,
                 "chrom": chrom,
                 "start": start,
                 "end": end,
                 "ref_fn": data["reference"],
                 "chemistry": data["chemistry"],
                 "genome_size": int(end) - int(start),
                 "threads": data["threads"],
                 "memory": data["memory"],
                 "unpartioned_bam_fn": data["ambiguous_bamfn"],
                 "subreads_xml": data["pacbio_reads_xml"],
                 "assembly_type": data["assembly_type"],
                 "gap_filling": data["gap_filling"],
                 "stitch_contigs": data["stitch_contigs"],
                 "clean_contigs": data["clean_contigs"],
                 "working_directory": outdir
                 }
        tmpl_bash("assembly",swap,bashfn)
        bashfns.append(bashfn)
    return bashfns

def combine_windows(interval_output,hap_windows):
    raw_records = []
    quivered_records = []
    for chrom,start,end in hap_windows:
        outdir = "%s/%s/%s_%s" % (interval_output,chrom,start,end)
        raw_contigs_fasta = "%s/raw.contigs.fasta" % outdir
        if os.path.isfile(raw_contigs_fasta):
            for record in SeqIO.parse(raw_contigs_fasta,'fasta'):
                record.id = "%s_%s_%s_%s" % (chrom,start,end,record.id)
                raw_records.append(record)
        quiver_contigs_fasta = "%s/quiver.contigs.fasta" % outdir
        if os.path.isfile(quiver_contigs_fasta):
            for record in SeqIO.parse(quiver_contigs_fasta,'fasta'):
                record.id = "%s_%s_%s_%s" % (chrom,start,end,record.id)
                quivered_records.append(record)
    if len(raw_records) != 0:
        out_raw_fasta = "%s/%s_raw.contigs.fasta" % (interval_output,chrom)
        SeqIO.write(raw_records,out_raw_fasta,"fasta")
    if len(quivered_records) != 0:
        out_quiver_fasta = "%s/%s_quivered.contigs.fasta" % (interval_output,chrom)
        SeqIO.write(quivered_records,out_quiver_fasta,"fasta")
