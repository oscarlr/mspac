#!/bin/env python
from pomegranate import *
from Bio import AlignIO
import numpy as np

observations = {
    "3": { 
        "A" : { "A": { "A": 0,"T": 2,"C": 2,"G": 2,"N": 14,"-": 10},
                "T": { "A": 3,"T": 1,"C": 4,"G": 4,"N": 14,"-": 11},
                "C": { "A": 3,"T": 4,"C": 1,"G": 4,"N": 14,"-": 11},
                "G": { "A": 3,"T": 4,"C": 4,"G": 1,"N": 14,"-": 11},
                "N": { "A": 14,"T": 14,"C": 14,"G": 14,"N": 14,"-": 14},
                "-": { "A": 7,"T": 8,"C": 8,"G": 8,"N": 14,"-": 5}
                },
        "T" : { "T": { "T": 0,"C": 2,"G": 2,"A": 2,"N": 14,"-": 10},
                "C": { "T": 3,"C": 1,"G": 4,"A": 4,"N": 14,"-": 11},
                "G": { "T": 3,"C": 4,"G": 1,"A": 4,"N": 14,"-": 11},
                "A": { "T": 3,"C": 4,"G": 4,"A": 1,"N": 14,"-": 11},
                "N": { "T": 14,"C": 14,"G": 14,"A": 14,"N": 14,"-": 14},
                "-": { "T": 7,"C": 8,"G": 8,"A": 8,"N": 14,"-": 5}
                },
        "C" : { "C": { "C": 0,"G": 2,"A": 2,"T": 2,"N": 14,"-": 10},
                "G": { "C": 3,"G": 1,"A": 4,"T": 4,"N": 14,"-": 11},
                "A": { "C": 3,"G": 4,"A": 1,"T": 4,"N": 14,"-": 11},
                "T": { "C": 3,"G": 4,"A": 4,"T": 1,"N": 14,"-": 11},
                "N": { "C": 14,"G": 14,"A": 14,"T": 14,"N": 14,"-": 14},
                "-": { "C": 7,"G": 8,"A": 8,"T": 8,"N": 14,"-": 5}
                },
        "G" : { "G": { "G": 0,"A": 2,"T": 2,"C": 2,"N": 14,"-": 10},
                "A": { "G": 3,"A": 1,"T": 4,"C": 4,"N": 14,"-": 11},
                "T": { "G": 3,"A": 4,"T": 1,"C": 4,"N": 14,"-": 11},
                "C": { "G": 3,"A": 4,"T": 4,"C": 1,"N": 14,"-": 11},
                "N": { "G": 14,"A": 14,"T": 14,"C": 14,"N": 14,"-": 14},
                "-": { "G": 7,"A": 8,"T": 8,"C": 8,"N": 14,"-": 5}
                },
        "N" : { "G": { "G": 14,"A": 14,"T": 14,"C": 14,"N": 14,"-": 14},
                "A": { "G": 14,"A": 14,"T": 14,"C": 14,"N": 14,"-": 14},
                "T": { "G": 14,"A": 14,"T": 14,"C": 14,"N": 14,"-": 14},
                "C": { "G": 14,"A": 14,"T": 14,"C": 14,"N": 14,"-": 14},
                "N": { "G": 14,"A": 14,"T": 14,"C": 14,"N": 14,"-": 14},
                "-": { "G": 14,"A": 14,"T": 14,"C": 14,"N": 14,"-": 14}
                },
        "-" : { "-": { "-": None,"A": 12,"T": 12,"C": 12,"G": 12,"N": 14},
                "A": { "-": 9,"A": 6, "T": 13, "C": 13,"G": 13,"N": 14},
                "T": { "-": 9,"A": 13,"T": 6,"C": 13,"G": 13,"N": 14},
                "C": { "-": 9,"A": 13,"T": 13,"C": 6,"G": 13,"N": 14},
                "G": { "-": 9,"A": 13,"T": 13,"C": 13,"G": 6,"N": 14},
                "N": { "-": 14,"A": 14,"T": 14,"C": 14,"G": 14,"N": 14}
                }
        },
    "2": {
        "A" : { "A": 1, "-": 0, "G": 3, "C": 3, "T": 3, "N": 3},
        "T" : { "T": 1, "A": 3, "-": 0, "G": 3, "C": 3, "N": 3},
        "C" : { "C": 1, "T": 3, "A": 3, "-": 0, "G": 3, "N": 3},
        "G" : { "G": 1, "C": 3, "T": 3, "A": 3, "-": 0, "N": 3},
        "-" : { "-": None, "G": 2, "C": 2, "T": 2, "A": 2, "N": 3},
        "N" : { "-": 3, "G": 3, "C": 3, "T": 3, "A": 3, "N": 3}
        }
    }

def state_probs(index):
    prob_for_index = .99/len(index)
    probs = [.003]*15
    for i in index:
        probs[i] = prob_for_index
    obs = list(range(0,15))
    return dict(zip(obs,probs))

def three_hmm():
    model = HiddenMarkovModel("Sequence Aligner")
    ins_hom = State(DiscreteDistribution(state_probs([10])), name="INS_1|1")
    ins_het1 = State(DiscreteDistribution(state_probs([5])), name="INS_1|0")
    ins_het2 = State(DiscreteDistribution(state_probs([9])), name="INS_0|1")
    del_hom = State(DiscreteDistribution(state_probs([12])), name="DEL_1|1")
    del_het1 = State(DiscreteDistribution(state_probs([6])), name="DEL_1|0")
    del_het2 = State(DiscreteDistribution(state_probs([7])), name="DEL_0|1")
    com = State(DiscreteDistribution(state_probs(list(range(0,15)))), name="COMPLEX_.|.")
    normal = State(DiscreteDistribution(state_probs([0])), name="NORMAL")
    all_states = [ins_hom,ins_het1,ins_het2,del_hom,del_het1,del_het2,com,normal]
    model.add_states(all_states)
    for state in all_states:
        if state.name != "NORMAL":
            model.add_transition(model.start, state, 0)
            model.add_transition(state, model.end, 0)
        else:
            model.add_transition(model.start, state, 1)
            model.add_transition(state, model.end, 1)
    trans_probs = np.zeros((8,8))
    trans_probs.fill(1e-15)
    np.fill_diagonal(trans_probs,.99)
    trans_probs[:,len(trans_probs)-1] = 1e-3
    trans_probs[len(trans_probs)-1] = [4.5e-15,4.5e-15,4.5e-15,4.5e-15,4.5e-15,4.5e-15,1.1e-15,(1-2e-15)]
    for state, t_prob in zip(all_states,trans_probs):
        for s, prob in zip(all_states,t_prob):
            model.add_transition(state,s,prob)
    model.add_transition(normal,normal,(1-2e-15))
    model.bake()
    return model

def two_hmm():
    model = HiddenMarkovModel("aligner")
    ins = State(DiscreteDistribution({0:.99, 1:.003, 2:.003, 3:.004}), name="INS")
    deletion = State(DiscreteDistribution({0:.003, 1:.003, 2:.99, 3:.004}), name="DEL")
    complex_ = State(DiscreteDistribution({0:.25, 1:.25, 2:.25, 3:.25}), name="COMPLEX") 
    normal = State(DiscreteDistribution({0:.003, 1:.99, 2:.003, 3:.004}), name="NORMAL") 
    all_states = [ins,deletion,complex_,normal]
    model.add_states(all_states)
    for state in all_states:
        if state.name != "NORMAL":
            model.add_transition(model.start, state, 0)
            model.add_transition(state, model.end, 0)
        else:
            model.add_transition(model.start, state, 1)
            model.add_transition(state, model.end, 1)
    trans_probs = [[0.999,1e-15,1e-15,1e-3],
                   [1e-15,0.999,1e-15,1e-3],
                   [1e-15,1e-33,0.999,1e-3],
                   [4.5e-15,4.5e-15,1.1e-15,(1-2e-15)]]
    trans_probs[len(trans_probs)-1][-1] = 1-2e-15
    for state, t_prob in zip(all_states,trans_probs):
        for s, prob in zip(all_states,t_prob):
            model.add_transition(state,s,prob)
    model.bake()
    return model

model = { 
    "3": three_hmm(),
    "2": two_hmm()
    }
