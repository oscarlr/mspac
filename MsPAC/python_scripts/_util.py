#!/bin/env python
from string import Template
import os
from MsPAC.cluster.lsf import Lsf

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def create_dir(dirname):
    if not os.path.exists(dirname):
        os.makedirs(dirname)

def tmpl_bash(tmplfn,swap,bashfn):
    filein = open("%s/bash_scripts/%s.sh" % ("/".join(os.path.dirname(__file__).split("/")[:-1]),tmplfn))
    src = Template(filein.read())
    output_lines = src.safe_substitute(swap)
    bashfh = open(bashfn,'w')
    bashfh.write(output_lines)
    filein.close()
    bashfh.close()

def sort_bams(bam_prefix,threads=1):
    bashfn = "%s.sh" % bam_prefix
    swap = { "prefix": bam_prefix,
             "threads": threads
             }
    tmpl_bash("sort_bams",swap,bashfn)
    os.system("sh %s" % bashfn)
    
def load_bed_intervals(bed_file):
    intervals = []
    with open(bed_file,'r') as f:
        for line in f:
            intervals.append(line.rstrip().split('\t')[0:3])
    return intervals

def save_intervals(intervals,bed_fn):
    with open(bed_fn,'w') as f:
        for chrom,start,end in intervals:
            f.write("%s\t%s\t%s\n" % (chrom,start,end))

def submit_bashfiles(input_data,bashfns):
    print "in submit_bashfiles"
    if len(bashfns) == 0:
        return
    if input_data["cluster"] == "False":
        for bashfn in bashfns:
            os.system("sh %s" % bashfn)
        return
    if input_data["memory"] != None:
        mem = int(input_data["memory"])
    if input_data["num_jobs"] != None:
        n = int(input_data["num_jobs"])
    if input_data["threads"] != None:
        threads = int(input_data["threads"])
    if input_data["walltime"] != None:
        walltime = int(input_data["walltime"])
    if input_data["queue"] != None:
        queue = input_data["queue"]
    hpc = Lsf()
    hpc.config(cpu=threads,walltime=walltime,memory=mem,queue=queue)
    if n > 1:
        bashs = chunks(bashfns,n)
        for chunk in bashs:
            outfn = hpc.set_job()
            for bash_fn in chunk:
                hpc.combine_jobs(outfn,bash_fn)
            hpc.submit("%s" % outfn)
    else:
        for bash_fn in bashfns:
            hpc.submit("%s" % bash_fn)
    hpc.wait()
