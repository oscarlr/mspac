#!/bin/env python
import sys
import numpy as np
import os
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from MsPAC.python_scripts._util import tmpl_bash

def get_chrom_length(m5_fn):
    with open(m5_fn,'r') as fn:
        for line in fn:
            chrom = line.rstrip().rsplit()[5]
            max_length = int(line.rstrip().rsplit()[6])
            return (chrom,max_length)

def alignment_locations(chrom_length,m5_fn,input_chrom):
    overlaps = np.array([0]*chrom_length)
    with open(m5_fn,'r') as fn:
        for line in fn:
            chrom = line.rstrip().rsplit()[5]
            if input_chrom == chrom:
                ref_align_start = int(line.rstrip().rsplit()[7])
                ref_align_end = int(line.rstrip().rsplit()[8])
                overlaps[ref_align_start:ref_align_end] += 1
    overlaps[overlaps > 1] = 0
    return overlaps

def group(L):
    first = last = L[0]
    for n in L[1:]:
        if n - 1 == last: # Part of the group, bump the end
            last = n
        else: # Not part of the group, yield current group and start a new
            yield first, last
            first = last = n
    yield first, last

def get_overlap(a, b):
    return max(0, min(a[1], b[1]) - max(a[0], b[0]))

def hap_msa_sequence(m5_fn,start,end,chrom):
    t_strand = None
    q_strand = None
    ref_sequence = []
    hap_sequence = []
    with open(m5_fn,'r') as fn:
        for line in fn:
            input_chrom = line.rstrip().rsplit()[5]
            ref_align_start = int(line.rstrip().rsplit()[7])
            ref_align_end = int(line.rstrip().rsplit()[8])
            if get_overlap([start,end],[ref_align_start,ref_align_end]) > 0 and chrom == input_chrom:
                t_strand = line.rstrip().rsplit()[4]
                q_strand = line.rstrip().rsplit()[9]
                q_aligned_seq = line.rstrip().rsplit()[16]
                match_pattern = line.rstrip().rsplit()[17]
                t_aligned_seq = line.rstrip().rsplit()[18]
                if q_strand == "-":
                    q_aligned_seq = Seq(q_aligned_seq).reverse_complement()
                    t_aligned_seq = Seq(t_aligned_seq).reverse_complement()
                current_ref_align = int(line.rstrip().rsplit()[7])
                for q,m,t in zip(q_aligned_seq,match_pattern,t_aligned_seq):
                    if current_ref_align >= start and current_ref_align <= end:
                        if t != "-":
                            ref_sequence.append(t)
                        if q != "-":
                            hap_sequence.append(q)
                    if t != "-":
                        current_ref_align += 1
                break
    ref_sequence = "".join(ref_sequence)
    hap_sequence = "".join(hap_sequence)
    assert t_strand != "-"
    return (ref_sequence,hap_sequence)

def fasta_sequence_single_hap(data,hap_locations,m5_fn,hap):
    sequences = {}
    for start, end in hap_locations:
        ref_sequence, hap_sequence = hap_msa_sequence(data[m5_fn],start,end,data["chromosome"])
        sequences[(data["chromosome"],start,end)] = { "%s_%s_%s" % (data["chromosome"],start,end): ref_sequence,
                                                      hap: hap_sequence                               
                                                      }
    return sequences

def get_intervals(overlaps,val):
    indexes = np.where(overlaps == val)
    groupings = list(group(indexes[0]))
    return groupings

def hap_overlap(hap1_locations,hap2_locations):
    overlap = hap1_locations + hap2_locations
    intervals = get_intervals(overlap,2)
    return intervals

def get_overlap_sequences(data,hap_overlap_locations):
    sequences = {}
    for start, end in hap_overlap_locations:
        hap1_ref_sequence, hap1_sequence = hap_msa_sequence(data["hap1_m5_fn"],start,end,data["chromosome"])
        hap2_ref_sequence, hap2_sequence = hap_msa_sequence(data["hap2_m5_fn"],start,end,data["chromosome"])
        assert hap1_ref_sequence == hap2_ref_sequence
        sequences[(data["chromosome"],start,end)] = { "%s_%s_%s" % (data["chromosome"],start,end): hap1_ref_sequence,
                                                      "H1": hap1_sequence,
                                                      "H2": hap2_sequence
                                                      }
    return sequences
    
def get_bashscripts(data,overlap_sequences,haplotypes):
    bashscripts = []
    output_fastas = "%s/sv_detection/%s/fastas" % (data["output_directory"],data["chromosome"])
    output_clus = "%s/sv_detection/%s/clu" % (data["output_directory"],data["chromosome"])
    output_beds = "%s/sv_detection/%s/bed" % (data["output_directory"],data["chromosome"])
    output_bashs = "%s/sv_detection/%s/bashs" % (data["output_directory"],data["chromosome"])
    for interval in overlap_sequences:
        ref_name = "%s_%s_%s" % (interval[0],interval[1],interval[2])
        ref_seq = SeqRecord(Seq(overlap_sequences[interval][ref_name]),
                            id=ref_name,name=ref_name)
        h_seq = SeqRecord(Seq(overlap_sequences[interval][haplotypes[0]]),
                           id=haplotypes[0],name=haplotypes[0])
        if len(haplotypes) == 2:
            h2_seq = SeqRecord(Seq(overlap_sequences[interval][haplotypes[1]]),
                               id=haplotypes[1],name=haplotypes[1])
            records = [ref_seq,h_seq,h2_seq]
        elif len(haplotypes) == 1:
            records = [ref_seq,h_seq]
        outfasta = "%s/%s.fasta" % (output_fastas,ref_name)
        print outfasta
        SeqIO.write(records, outfasta, "fasta")
        outclu = "%s/%s.clu" % (output_clus,ref_name)
        outbed = "%s/%s.bed" % (output_beds,ref_name)
        bashfn = "%s/%s.sh" % (output_bashs,ref_name)
        swap = { "outfn_clu": outclu,
                 "outfn_fa": outfasta,
                 "outfn_bed": outbed,
                 "chrom": data["chromosome"],
                 "start": interval[1],
                 "end": interval[2]
                 }
        if os.path.isfile("%s" % outbed):
            if os.stat("%s" % outbed).st_size != 0:
                continue
        if os.path.isfile("%s.done" % outbed):
            continue
        tmpl_bash("msa_and_sv_detect",swap,bashfn)
        bashscripts.append(bashfn)
    return bashscripts

def setup_dirs(data):
    output_fastas = "%s/sv_detection/%s/fastas" % (data["output_directory"],data["chromosome"])
    output_clus = "%s/sv_detection/%s/clu" % (data["output_directory"],data["chromosome"])
    output_beds = "%s/sv_detection/%s/bed" % (data["output_directory"],data["chromosome"])
    output_bashs = "%s/sv_detection/%s/bashs" % (data["output_directory"],data["chromosome"])
    if not os.path.exists(output_fastas):
        os.makedirs(output_fastas)
    if not os.path.exists(output_clus):
        os.makedirs(output_clus)
    if not os.path.exists(output_beds):
        os.makedirs(output_beds)
    if not os.path.exists(output_bashs):
        os.makedirs(output_bashs)    

def msa_sv(data):
    bashscripts = []
    setup_dirs(data)
    if data["hap2_m5_fn"] == None:
        pass
    m5_chrom,chrom_length = get_chrom_length(data["hap1_m5_fn"])
    assert m5_chrom == data["chromosome"]
    hap1_locations = alignment_locations(chrom_length,data["hap1_m5_fn"],m5_chrom)
    hap2_locations = alignment_locations(chrom_length,data["hap2_m5_fn"],m5_chrom)    
    hap_overlap_locations = hap_overlap(hap1_locations,hap2_locations)
    hap1_only_locations = get_intervals(hap1_locations - hap2_locations,1)
    hap2_only_locations = get_intervals(hap2_locations - hap1_locations,1)
    assert len(set(hap_overlap_locations).intersection(set(hap1_only_locations))) == 0
    assert len(set(hap_overlap_locations).intersection(set(hap2_only_locations))) == 0
    assert len(set(hap1_only_locations).intersection(set(hap2_only_locations))) == 0
    overlap_sequences = get_overlap_sequences(data,hap_overlap_locations)
    hap1_only_sequence = fasta_sequence_single_hap(data,hap1_only_locations,"hap1_m5_fn","H1")
    hap2_only_sequence = fasta_sequence_single_hap(data,hap2_only_locations,"hap2_m5_fn","H2")
    overlap_bashes = get_bashscripts(data,overlap_sequences,["H1","H2"])
    h1_bashes = get_bashscripts(data,hap1_only_sequence,["H1"])
    h2_bashes = get_bashscripts(data,hap2_only_sequence,["H2"])
    for bash in overlap_bashes:
        bashscripts.append(bash)
    for bash in h1_bashes:
        bashscripts.append(bash)
    for bash in h2_bashes:
        bashscripts.append(bash)
    return bashscripts 

