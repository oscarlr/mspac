#!/bin/env python
import sys
import json
import os
from python_scripts.partition import split_bam_by_phased_snps
from python_scripts._util import sort_bams,load_bed_intervals,submit_bashfiles
from python_scripts.assemble import coverage_regions,split_regions_into_windows,assemble_intervals,combine_windows
from python_scripts.structural_variation_detection import msa_sv

def run_partition(input_data):
    output = "%s/partition/%s" % (input_data["output_directory"],
                                  input_data["chromosome"])
    if not os.path.exists(output):
        os.makedirs(output)
    done_partitioning_file = "%s/done.partitioning" % output
    if not os.path.isfile(done_partitioning_file):
        split_bam_by_phased_snps(input_data)
        with open(done_partitioning_file,"w") as f:
            pass
    sort_bams("%s/hap1" % output)
    sort_bams("%s/hap2" % output)
    sort_bams("%s/snpdesert" % output)

def run_assembly(input_data):
    output = "%s/assembly" % input_data["output_directory"]
    if not os.path.exists(output):
        os.makedirs(output)
    hap_windows = {}
    hap_names = [["hap1","haplotype1_bamfn"]]
    if input_data["haplotype2_bamfn"] != "None":
        hap_names.append(["hap2","haplotype2_bamfn"])
    if input_data["bed_file"] == "None":
        for hap_name, hap_bamfn in hap_names:
            output = "%s/assembly/%s" % (input_data["output_directory"],hap_name)
            hap_coverage_regions = coverage_regions(output,input_data[hap_bamfn],input_data)
            windows = split_regions_into_windows(output,hap_coverage_regions,input_data)
            hap_windows[hap_name] = windows
    else:
        hap1_coverage_regions = load_bed_intervals(input_data["bed_file"])
        #hap1_windows = split_regions_into_windows(output,hap1_coverage_regions,input_data)
        hap1_windows = hap1_coverage_regions
        hap_windows["hap1"] = hap1_windows
        if input_data["haplotype2_bamfn"] != "None":
            hap_windows["hap2"] = hap1_windows
    all_bashfns = []
    for hap_name, hap_bamfn in hap_names:
        interval_output = "%s/assembly/%s" % (input_data["output_directory"],hap_name)
        bashfns = assemble_intervals(interval_output,hap_windows[hap_name],input_data[hap_bamfn],input_data)    
        for fn in bashfns:
            all_bashfns.append(fn)
    submit_bashfiles(input_data,all_bashfns)
    for hap_name, hap_bamfn in hap_names:
        interval_output = "%s/assembly/%s" % (input_data["output_directory"],hap_name)
        combine_windows(interval_output,hap_windows[hap_name])

def run_sv_detection(input_data):
    output = "%s/sv_detection" % input_data["output_directory"]
    if not os.path.exists(output):
        os.makedirs(output)
    bashscripts = msa_sv(input_data)
    submit_bashfiles(input_data,bashscripts)

def main():
    if len(sys.argv) == 1:
        sys.exit("Pass in JSON file")
    json_file = sys.argv[1]
    with open(json_file,'rb') as json_fh:
        input_data = json.load(json_fh)
        for key in input_data:
            input_data[key] = str(input_data[key])
        if not os.path.exists(input_data["output_directory"]):
            os.makedirs(input_data["output_directory"])
        if input_data["program"] == "partition":
            run_partition(input_data)
        elif input_data["program"] == "assemble":
            run_assembly(input_data)
        elif input_data["program"] == "sv-detection":
            run_sv_detection(input_data)

if __name__ == '__main__':
    sys.exit(main())
