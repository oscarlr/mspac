#!/bin/bash
set -e -x

samtools sort -@ ${threads} ${prefix}.bam ${prefix}.sorted
samtools index ${prefix}.sorted.bam
rm -f ${prefix}.bam
