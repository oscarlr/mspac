#!/bin/bash
set -e -x

outfn_clu=$1
outfn_fa=$2
outfn_bed=$3
chrom=$4
start=$5
end=$6
outfn_bed=$7

if [ ! -s ${outfn_clu} ]
then
    kalign -f clu -s 100 -e 0.85 -t 0.45 -m 0 -in ${outfn_fa} | sed 's/Kalign/CLUSTAL/g' > ${outfn_clu}
fi

if [ ! -s ${outfn_bed} ]
then
    msa_to_variants.py ${outfn_clu} ${chrom} ${start} ${end} > ${outfn_bed}
    echo "" > ${outfn_bed}.done
fi


