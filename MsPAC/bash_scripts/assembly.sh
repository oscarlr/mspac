#!/bin/bash
set -e -x

bam_fn=$1
chrom=$2
start=$3
end=$4
ref_fn=$5
genome_size=$6
threads=$7
memory=$8
unpartioned_bam_fn=$9
subreads_xml=${10}
assembly_type=${11}
working_directory=${12}

cd ${working_directory}

function extract_reads_and_ref {
    samtools view ${bam_fn} ${chrom}:${start}-${end} | awk '{OFS="\t"; print ">"$1"\n"$10}' - > reads.fasta
    samtools faidx ${ref_fn} ${chrom}:${start}-${end} > ref.fa
}

function assemble_conservative {
    canu -p raw -d canu contigFilter="2 1000 1.0 1.0 2" corMinCoverage=0 genomeSize=${genome_size} errorRate=0.035 useGrid=0 maxThreads=${threads} maxMemory=${memory} -pacbio-raw reads.fasta
    if [ -s canu/raw.contigs.fasta ]
    then
	cp canu/raw.contigs.fasta raw.contigs.fasta	
    else
	echo "" > no.raw.contigs
	exit 0
    fi
    rm -fr canu
}

function assemble_aggressive {
    cat reads.fasta > reads2.fasta
    samtools view ${unpartioned_bam_fn} ${chrom}:${start}-${end} | awk '{OFS="\t"; print ">"$1"\n"$10}' - >> reads2.fasta
    canu -p raw -d canu2 contigFilter="2 1000 1.0 1.0 2" corMinCoverage=0 genomeSize=${genome_size} errorRate=0.035 useGrid=0 maxThreads=${threads} maxMemory=${memory} -pacbio-raw reads2.fasta
    if [ -s canu2/raw.contigs.fasta ]
    then
        cp canu2/raw.contigs.fasta raw.contigs.fasta
    else
        minimap -Sw5 -L100 -m0 canu/raw.trimmedReads.fasta.gz canu/raw.trimmedReads.fasta.gz | gzip -1 > canu/raw.trimmedReads.paf.gz
        miniasm -c 1 -e 2 -f canu/raw.trimmedReads.fasta.gz canu/raw.trimmedReads.paf.gz > canu/raw.trimmedReads.gfa
        cat canu/raw.trimmedReads.gfa | awk '$1 == "S"' | awk '{ print ">"$2"\n"$3 }' > canu/raw.contigs.fasta
        if [ -s canu/raw.contigs.fasta ]
        then
            cp canu/raw.contigs.fasta raw.contigs.fasta
        else
            minimap -Sw5 -L100 -m0 canu2/raw.trimmedReads.fasta.gz canu2/raw.trimmedReads.fasta.gz | gzip -1 > canu2/raw.trimmedReads.paf.gz
            miniasm -c 1 -e 2 -f canu2/raw.trimmedReads.fasta.gz canu2/raw.trimmedReads.paf.gz > canu2/raw.trimmedReads.gfa
            cat canu2/raw.trimmedReads.gfa | awk '$1 == "S"' | awk '{ print ">"$2"\n"$3 }' > canu2/raw.contigs.fasta
            if [ -s canu2/raw.contigs.fasta ]
            then
                cp canu2/raw.contigs.fasta raw.contigs.fasta
            else
                echo "No contig assembled"
                echo "" > interval.done
            fi
        fi
    fi
    rm -fr canu2
}

function fill_in_gaps {
    blasr -noSplitSubreads -nproc ${threads} -bestn 1 raw.contigs.fasta ref.fa -out raw.contigs.m5
    mkdir -p gaps
    echo -e "${chrom}\t${start}\t${end}" > gaps/region.bed
    awk '{ print "${chrom}\t"$8+${start}"\t"$9+${start} }' raw.contigs.m5 | sort -k1,1 -k2,2n | bedtools merge -i stdin > gaps/covered.bed
    bedtools subtract -a gaps/region.bed -b gaps/covered.bed > gaps/gaps.bed
    cat gaps/gaps.bed | while read chrom gap_start gap_end
    do
	size=`awk -v gap_start=${gap_start} -v gap_end=${gap_end} 'BEGIN{print gap_end-gap_start}'`
	if [ "${size}" -gt 5000 ]
	then
            new_start=`awk -v gap_start=${gap_start} 'BEGIN{ print gap_start - 5000}'`
            new_end=`awk -v gap_end=${gap_end} 'BEGIN{ print gap_end + 5000}'`
            if [ ! -f gaps/${new_start}_${new_end}/raw.contigs.fasta ]
            then
		mkdir -p gaps/${new_start}_${new_end}
		cd gaps/${new_start}_${new_end}
		samtools view ${bam_fn} ${chrom}:${new_start}-${new_end} | awk '{OFS="\t"; print ">"$1"\n"$10}' - > reads.fasta
		samtools faidx ${ref_fn} ${chrom}:${new_start}-${new_end} > ref.fa
		canu -p raw -d canu contigFilter="2 1000 1.0 1.0 2" corMinCoverage=0 genomeSize=${size} errorRate=0.035 useGrid=0 maxThreads=${threads} maxMemory=${memory} -pacbio-raw reads.fasta
		if [ -s canu/raw.contigs.fasta ]
		then
		    cp canu/raw.contigs.fasta raw.contigs.fasta
		fi
		rm -fr canu
	    fi
	fi
    done

}

function merge_contigs {
    rm -fr amos/
    mkdir -p amos
    cat raw.contigs.fasta > amos/raw.contigs.fasta
    ls gaps/*/raw.contigs.fasta | while read fn
    do
	gap_coord=`echo ${fn} | cut -f2 -d "/"`
	cat ${fn} | sed "s/>/>${gap_coord}_/g"
    done >> amos/raw.contigs.fasta
    #cat gaps/*/raw.contigs.fasta | sed 's/>/>gap_/g' 
    toAmos -s amos/raw.contigs.fasta -o amos/contigs.amos.afg
    minimus amos/contigs.amos.afg
    listReadPlacedStatus -S -E amos/contigs.amos.bnk > amos/contigs.amos.singletons
    dumpreads -e -E amos/contigs.amos.singletons amos/contigs.amos.bnk > amos/contigs.amos.singletons.fasta
    cat amos/contigs.amos.fasta | fasta_formatter -w 0 > amos/tmp.contigs.amos.total.fasta
    cat amos/contigs.amos.singletons.fasta | fasta_formatter -w 0 >> amos/tmp.contigs.amos.total.fasta
    cat amos/tmp.contigs.amos.total.fasta | awk '{print (NR%2 == 1) ? ">" ++i : $0}' > amos/contigs.amos.total.fasta
    cp amos/contigs.amos.total.fasta raw.contigs.fasta
}

function quiver_contigs {
    qname=`cat reads.fasta | grep ">" | sed 's/>/qname=/g' | tr '\n' ' '`
    dataset filter ${subreads_xml} reads.xml ${qname}
    blasr -nproc ${threads} -bestn 1 reads.xml raw.contigs.fasta -out raw.contigs.bam -bam
    #blasr -nproc ${threads} -bestn 1 ${subreads_xml} raw.contigs.fasta -out raw.contigs.bam -bam
    samtools sort raw.contigs.bam raw.contigs.sorted
    samtools faidx raw.contigs.fasta
    pbindex raw.contigs.sorted.bam
    quiver raw.contigs.sorted.bam -j ${threads} -p ${chemistry} -r raw.contigs.fasta -o quiver.contigs.fasta -o quiver.contigs.fastq
}

extract_reads_and_ref

if [ ! -s raw.contigs.fasta ]
then
    assemble_conservative
fi

if [ ! -s raw.contigs.fasta ]
then
    if [ "${assembly_type}" = "aggressive" ]
    then
	assemble_aggressive
    fi
fi

if [ "${gap_filling}" = "True" ]
then
    fill_in_gaps
fi

if [ "${stitch_contigs}" = "True" ]
then
    num_original_contigs=`cat raw.contigs.fasta | grep ">" | wc -l`
    if ls gaps/*/raw.contigs.fasta 1> /dev/null 2>&1
    then
	mkdir -p amos
	merge_contigs
    elif [ "${num_original_contigs}" -ge 1 ]
    then
	mkdir -p amos
	merge_contigs
    else
	x=1
    fi
fi

if [ ! -s quiver.contigs.fasta ]
then
    if [ "${clean_contigs}" = "True" ]
    then
	quiver_contigs
    fi
fi
