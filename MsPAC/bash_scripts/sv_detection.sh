#!/bin/bash

#/hpc/users/bashia02/gitrepos/mchaisson7/blasr/alignment/bin                                                                                                                                              
cd ${outdir}

${FAST_BLASR}/blasr \
    -alignContigs \
    -noSplitSubreads \
    -bestn 1 \
    -clipping soft \
    -nproc ${threads} \
    ${hap1_fasta} \
    ${ref} \
    -out hap1.m5 -m 5 

if [ "${hap2_fasta}" -eq "None" ]
then
    ${FAST_BLASR}/blasr \
	-alignContigs \
	-noSplitSubreads \
	-bestn 1 \
	-clipping soft \
	-nproc ${threads} \
	${hap2_fasta} \
	${ref} \
	-out hap2.m5 -m 5 
fi