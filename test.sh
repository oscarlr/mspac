#!/bin/bash
set -e -x
module load java/1.8.0_66 gcc glib python py_packages samtools BEDTools amos seqtk fastx
CANU_DIR=~/tools/canu-1.4/Linux-amd64/bin
SMRT=/sc/orga/projects/pacbio/modules/smrtanalysis/3.0.0/smrtsuite/smrtcmds/bin
MINIASM=/sc/orga/work/rodrio10/tools/miniasm
MINIMAP=/sc/orga/work/rodrio10/tools/minimap
export PATH=${SMRT}:${PATH}:${CANU_DIR}:${MINIASM}:${MINIMAP}

function install {
    source venv/bin/activate
    python setup.py install
}

function create_test_data {
    module load samtools bcftools
    mkdir -p test
    bam=/sc/orga/scratch/rodrio10/projects/znfinger_repeats/data/2017-05-25_splitting_genomes_into_haps/download_aligned_reads/NA19238/20160608.NA19238.PacBio.BLASR/NA19238.YRI.chr22.BLASR.20160525.PacBio.10x-phased.bam
    vcf=/sc/orga/scratch/rodrio10/projects/znfinger_repeats/data/2017-05-25_splitting_genomes_into_haps/phased_vcfs/NA19238/NA19238_WGS_phased_variants.vcf.gz
    samtools view -bhS ${bam} chr22:37186673-37286673 > test/test.bam
    samtools index test/test.bam
    bcftools view -r chr22:37186673-37286673 -O v ${vcf} > test/test.vcf
    bgzip -c test/test.vcf > test/test.vcf.gz
    tabix -p vcf test/test.vcf.gz
}

function partition {
    MsPAC-prep \
	--program partition \
	--aligned_reads_bam test/test.bam \
	--phased_snps_vcf test/test.vcf.gz \
	--vcf_sample_name NA19238 \
	--chromosome chr22 \
	--output_directory /sc/orga/work/rodrio10/software/MsPAC/test \
	> partition_chr22.json
    cat partition_chr22.json
    MsPAC partition_chr22.json
}

function assemble {
    MsPAC-prep \
	--program assemble \
	--chromosome chr22 \
	--reference /sc/orga/projects/bashia02b/GR38/hg38.fa \
	--output_directory /sc/orga/work/rodrio10/software/MsPAC/test \
	--haplotype1_bamfn `pwd`/test/partition/chr22/hap1.sorted.bam \
	--haplotype2_bamfn `pwd`/test/partition/chr22/hap2.sorted.bam \
	--ambiguous_bamfn `pwd`/test/partition/chr22/snpdesert.sorted.bam \
	> assemble_chr22.json
    MsPAC assemble_chr22.json
}

function assemble_aggressive {
    MsPAC-prep \
	--program assemble \
	--assembly_type aggressive \
	--chromosome chr22 \
	--reference /sc/orga/projects/bashia02b/GR38/hg38.fa \
	--output_directory /sc/orga/work/rodrio10/software/MsPAC/test \
	--haplotype1_bamfn `pwd`/test/partition/chr22/hap1.sorted.bam \
	--haplotype2_bamfn `pwd`/test/partition/chr22/hap2.sorted.bam \
	--ambiguous_bamfn `pwd`/test/partition/chr22/snpdesert.sorted.bam \
	> assemble_chr22.json
    MsPAC assemble_chr22.json
}

function assemble_aggressive_gap_filling {
    MsPAC-prep \
	--program assemble \
	--assembly_type aggressive \
	--gap_filling \
	--chromosome chr22 \
	--reference /sc/orga/projects/bashia02b/GR38/hg38.fa \
	--output_directory /sc/orga/work/rodrio10/software/MsPAC/test \
	--haplotype1_bamfn `pwd`/test/partition/chr22/hap1.sorted.bam \
	--haplotype2_bamfn `pwd`/test/partition/chr22/hap2.sorted.bam \
	--ambiguous_bamfn `pwd`/test/partition/chr22/snpdesert.sorted.bam \
	> assemble_chr22.json
    MsPAC assemble_chr22.json
}

function assemble_aggressive_gap_filling_merge {
    MsPAC-prep \
	--program assemble \
	--assembly_type aggressive \
	--gap_filling \
	--stitch_contigs \
	--chromosome chr22 \
	--reference /sc/orga/projects/bashia02b/GR38/hg38.fa \
	--output_directory /sc/orga/work/rodrio10/software/MsPAC/test \
	--haplotype1_bamfn `pwd`/test/partition/chr22/hap1.sorted.bam \
	--haplotype2_bamfn `pwd`/test/partition/chr22/hap2.sorted.bam \
	--ambiguous_bamfn `pwd`/test/partition/chr22/snpdesert.sorted.bam \
	> assemble_chr22.json
    MsPAC assemble_chr22.json
}

#install
#create_test_data
#partition
#assemble
#assemble_aggressive
#assemble_aggressive_gap_filling
#assemble_aggressive_gap_filling_merge