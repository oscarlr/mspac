# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

### How do I get set up? ###

* Summary of set up
```
git clone https://oscarlr@bitbucket.org/oscarlr/mspac.git
cd MsPAC
python setup.py install
```
* Configuration for Minerva/MSSM users
```
# Minerva users:
module load java/1.8.0_66 gcc glib python py_packages samtools BEDTools amos seqtk fastx
CANU_DIR=~/tools/canu-1.4/Linux-amd64/bin
SMRT=/sc/orga/projects/pacbio/modules/smrtanalysis/3.0.0/smrtsuite/smrtcmds/bin
MINIASM=/sc/orga/work/rodrio10/tools/miniasm
MINIMAP=/sc/orga/work/rodrio10/tools/minimap
export PATH=${SMRT}:${PATH}:${CANU_DIR}:${MINIASM}:${MINIMAP}
```
* Libraries/tools needed for non Minerva/MSSM users
```
# Non minerva/MSSM users:
java/1.8.0_66
gcc
glib
numpy
samtools
BEDTools 
amos 
seqtk 
fastx
Canu
SMRT analysis
miniasm
minimap
```
* How to run tests
```
sh test.sh
```
* Deployment instructions

MsPAC is broken into three parts: partitioning PacBio reads, assembling PacBio reads and calling structural variants. In order to run each part you have to pass in a JSON file with all the options specified. 

**Partitioning PacBio reads.**

Creating JSON file to partition reads.
```
MsPAC-prep --program partition 
	--aligned_reads_bam test/test.bam
	--phased_snps_vcf test/test.vcf.gz
	--vcf_sample_name NA19238
	--chromosome chr22
	--output_directory test
	> partition_chr22.json
```
The JSON file that was created from MsPAC-prep.
```
(venv) [rodrio10@minerva4 MsPAC]$ cat partition_chr22.json 
{
    "aligned_reads_bam": "test/test.bam",
    "ambiguous_bamfofn": null,
    "assembly_type": "conservative",
    "bed_file": null,
    "chemistry": null,
    "chromosome": "chr22",
    "flanking": 10000,
    "haplotype1_bamfofn": null,
    "haplotype2_bamfofn": null,
    "min_reads": 2,
    "min_window_size": 10000,
    "output_directory": "/sc/orga/work/rodrio10/software/MsPAC/test",
    "pacbio_reads_xml": null,
    "phased_snps_vcf": "test/test.vcf.gz",
    "program": "partition",
    "reference": null,
    "trim_quality": 30,
    "vcf_sample_name": "NA19238",
    "window_size": 250000
}
(venv) [rodrio10@minerva4 MsPAC]$
```
Partitioning PacBio reads into haplotypes
```
MsPAC partition_chr22.json
```
Creating JSON file to partition reads.
```
MsPAC-prep --program assemble \
        --chromosome chr22 \
        --reference /sc/orga/projects/bashia02b/GR38/hg38.fa \
        --output_directory /sc/orga/work/rodrio10/software/MsPAC/test \
        --haplotype1_bamfn `pwd`/test/partition/chr22/hap1.sorted.bam \
        --haplotype2_bamfn `pwd`/test/partition/chr22/hap2.sorted.bam \
        --ambiguous_bamfn `pwd`/test/partition/chr22/snpdesert.sorted.bam \
        > assemble_chr22.json
```
The JSON file that was created from MsPAC-prep.
```
(venv) [rodrio10@interactive1 MsPAC]$ cat assemble_chr22.json 
{
    "aligned_reads_bam": null,
    "ambiguous_bamfn": "/sc/orga/work/rodrio10/software/MsPAC/test/partition/chr22/snpdesert.sorted.bam",
    "assembly_type": "conservative",
    "bed_file": null,
    "chemistry": null,
    "chromosome": "chr22",
    "clean_contigs": false,
    "flanking": 10000,
    "gap_filling": false,
    "haplotype1_bamfn": "/sc/orga/work/rodrio10/software/MsPAC/test/partition/chr22/hap1.sorted.bam",
    "haplotype2_bamfn": "/sc/orga/work/rodrio10/software/MsPAC/test/partition/chr22/hap2.sorted.bam",
    "memory": "8",
    "min_reads": 2,
    "min_window_size": 10000,
    "output_directory": "/sc/orga/work/rodrio10/software/MsPAC/test",
    "pacbio_reads_xml": null,
    "phased_snps_vcf": null,
    "program": "assemble",
    "reference": "/sc/orga/projects/bashia02b/GR38/hg38.fa",
    "stitch_contigs": false,
    "threads": "1",
    "trim_quality": 30,
    "vcf_sample_name": null,
    "window_size": 250000
}
```
Assembling PacBio reads
```
MsPAC assemble_chr22.json
```