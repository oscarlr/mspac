from setuptools import setup, find_packages
import numpy
include_path = [numpy.get_include()]

setup(
    name='MsPAC',
    description='Assembling diploid genome and calling structural variants',
    packages=find_packages(),
    include_package_data=True,
    scripts = ['MsPAC/python_scripts/MsPAC-prep'],
    entry_points = {
        'console_scripts': ['MsPAC = MsPAC.MsPAC:main'],
        },
    platforms='any'
)
